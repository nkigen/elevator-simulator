/*
 * elevator.h
 *
 *  Created on: 6 Feb 2013
 *      Author: nkigen
 */

#ifndef ELEVATOR_H_
#define ELEVATOR_H_
#define ALLEGRO_NO_MAGIC_MAIN
#include <allegro.h>
#include<stdio.h>
#include<math.h>
#define MAX_TEXT_LEN 150
#define ELEVATOR_ACC_MOVING 0.8
#define ELEVATOR_ACC_STOPPING 0.2
#define CABIN_THOLD 2
#define BUILDING_COLOR makecol(255, 0, 0)
#define FLOOR_COLOR makecol(255, 255, 255)
#define ELEVATOR_COLOR makecol(255, 255, 255)
#define CABIN_COLOR makecol(255, 255, 0)
#define PERSON_IN_CABIN_COLOR  makecol(0, 200, 20)
#define PERSON_IN_FLOOR_COLOR makecol(0, 200, 20)
#define LINE_COLOR makecol(255, 0, 0)
#define TEXT_COLOR makecol(255, 0, 0)
#define DEFAULT_NUM_FLOORS 15
#define MINIMUM_NUM_FLOORS 3
#define Y_LEN 600
#define X_LEN 800
#define MAX_CAPACITY 4
#define INIT_FLOOR_LEFT 0
#define INIT_FLOOR_RIGHT (NUM_FLOORS-1)
#define DIRECTION_UP 1
#define DIRECTION_DOWN 0
#define DIRECTION_NONE 2
#define LEFT_ELEVATOR 0
#define RIGHT_ELEVATOR 1
#define MAX_CAPACITY_FLOOR 10
#define ELEVATOR_MOVING 1
#define ELEVATOR_STOP 0
#define REQUEST_PENDING 0
#define REQUEST_SERVED 1
#define MAX_REQUESTS DEFAULT_NUM_FLOORS*MAX_CAPACITY_FLOOR
typedef struct r {
	unsigned int from;
	unsigned int to;
	unsigned int status;
} REQUEST;

typedef struct {
	unsigned int id;
	float x, y, w, h;
	unsigned int dest;
	REQUEST *mine;
} PERSON;

typedef struct {
	unsigned int id;
	float x, y;
	float w, h;
	unsigned int num_waiting;
	PERSON *people[MAX_CAPACITY_FLOOR];
} FLOOR;

typedef struct {
	PERSON *users[MAX_CAPACITY];
	float x, y, w, h;
	unsigned int init_floor;
	unsigned int current_floor;
	unsigned int direction;
	unsigned int max_capacity;
	unsigned int current_capacity;
	unsigned int status;
} CABIN;

typedef struct {
	CABIN cabin;
	float delay;
	float x, y, w, h;
unsigned int served_requests;
} ELEVATOR;

typedef struct {
	FLOOR *floors;
	ELEVATOR left_elevator, right_elevator;
	float w, h;
	REQUEST request_queue[MAX_REQUESTS];
	unsigned int pending_requests;
	unsigned int total_requests;
} BUILDING;

BUILDING building;
void buildingInit();
void drawBuilding();
void drawFloor();
void evelatorDisplay();
void drawCabin(float x, float y, float w, float h);
void updateCabinPosition(unsigned int elevator, unsigned int b_floor);
int addPersonToCabin(unsigned int elevator,unsigned int dest,REQUEST *r);
void addPersonToFloor(unsigned int current_floor,unsigned int dest,REQUEST *r);
void drawCabinContent();
void drawFloorContent();
void repaintPersonInElevator(PERSON *person);
void shiftPersonInElevator(PERSON *person,unsigned int elevator);
void slideElevator(unsigned int elevator);
#endif /* ELEVATOR_H_ */
